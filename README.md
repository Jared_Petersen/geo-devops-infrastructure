# Azure DevOps Infrastructure Templates

## How do I get started

1. Clone [Services Common](https://bitbucket.org/SEDSLN/geo-services-common/src/develop/)
2. Install [Visual Studio Code](https://code.visualstudio.com/download)
3. Install [Azure Pipelines for Vs CODE extension](https://github.com/Microsoft/azure-pipelines-vscode)

You can debug deployement pipelines with SE.Geospatial.Services.Common.Deployment project
Use the following command line

```dos
deploy --subscription=[Dev/Prod] --azure-client-id=TODO --azure-client-secret=TODO --api-key-client-id=TODO --api-key-client-secret=TODO -deployment-definition="C:\src\geo_src\geo-azure-functions\deploy\DeploymentDescription.yaml" --test
```

You will need to generate the clientid and secrets for out apis and microsofts. The --test will run the commend without making changes to any of our services, or creating any resources.

### Kv Storage ACL

```yml
-Name: MyAcl
 TenantKeyPolicies:
 - TenantId: '*'
   KeyPolicies:
   - Path: Folder/
     Permission: Read
   - Path: Folder/Key
     Permission: Write
```

### App Service

```yml
- Name: arcfm-[tenant]-[region][-dev]
    ResourceGroup: [tenant]
    Size: S1 # possible values B1,S1,S2,P1v2,P2v1
    Instances: 1
```

### Roles

- Add changes to dev, tenant specific changes in the prod section at bottom

```yml
- Name: Designer.Admin
  Category: Designer
  TenantScopes:
  - TenantId: '*'
    Scopes:
    - Access:CreateApiKey
    - Access:CreateUser
    - Access:DeleteApiKey
# or tenant specific this will append this tenant specific role to the general role
  - Name: Designer.Admin
    Category: Designer
    TenantScopes:
    - TenantId: NSPIPS
      Inherit: true
      Scopes:
      - CostEstimate:RequestEstimate
```

### Service Bus

```yml
  - Name: arcfm-[tenantId][-dev][-premium]
    RegionId: westus
    ResourceGroup: [tenantId]
    Topics:
    - Name: jobservices
      TTL: 20160 # add an optional time to live for messages
    - Name: kvupdates
    Queues:
    - Name: q.se.geospatial.designer.coordination.service.messages.pushtogis
    - Name: q.se.geospatial.designer.edgeservice.messages.createsession
```

### Tenancy

```yml
  - IsProductionTenant: true
    TenantAlias: Tenant Alias
    TenantId: TenantId
    Auth0Domain: https://arcfm[tenantID].auth0.com/
    ResourceGroup: [tenantID]
    Auth0Apps:
    - Name: ArcFM Connectivity Evaluator (OIDC)
# Can specify different callbacks here
      CallBacks:
      - com.schneiderelectric.arcfm.mobile://arcfmsolutiontest.auth0.com/ios/com.schneiderelectric.arcfm.mobile/callback
# Or grants  "authorization_code","client_credentials","implicit","refresh_token" are automatically added
      Grants:
      - http://auth0.com/oauth/grant-type/password-realm
    - Name: ArcFM Mobile (OIDC)
    - Name: ArcFM Mobile Portal (OIDC)
    - Name: Designer (OIDC)
    - Name: Editor (OIDC)
    - Name: SE Geospatial Services
    - Name: SE Geospatial Services (OIDC)
    - Name: Solution Center (OIDC)
    Services:
# wellknown services do not need a url this will use default values.
# Designer
# CuService,DesignerCoordination,DesignerEquipmentCatalog,PrintTemplatesService,
# Editor
# Editor,SessionManager,
# DHFC
# DHFCCatalog,
# GDBM 
# GeodatabaseManager
# Mobile
# NamedSearch,
# Core Services
# Tenancy,Access,KvStorage,Mapping,Monitoring,ServiceBusManagement
    - Name: Access
    - Name: CuService
    - Name: DesignerCoordination
    - Name: DesignerEquipmentCatalog
    - Name: Editor
    - Name: ESRI-Portal-OAuth[Tenant]
      ClientId: [ClientId]
      Url: https://[Portal]/portal/sharing/rest
    - Name: KvStorage
    - Name: Mapping
    - Name: MobileSync
      Url: https://arcfm-[tenantId]-mobile-sync.azurewebsites.net/
    - Name: Monitoring
    - Name: PrintTemplatesService
    - Name: Redlining
      Url: https://arcfm-[tenantId]-redlining.azurewebsites.net/
    - Name: ServiceBusManagement
    - Name: SessionManager
    - Name: Tenancy
```

### Regions

```powershell
Get-AzLocation | select DisplayName, Location | Format-Table

DisplayName          Location
-----------          --------
East Asia            eastasia
Southeast Asia       southeastasia
Central US           centralus
East US              eastus
East US 2            eastus2
West US              westus
North Central US     northcentralus
South Central US     southcentralus
North Europe         northeurope
West Europe          westeurope
Japan West           japanwest
Japan East           japaneast
Brazil South         brazilsouth
Australia East       australiaeast
Australia Southeast  australiasoutheast
South India          southindia
Central India        centralindia
West India           westindia
Canada Central       canadacentral
Canada East          canadaeast
UK South             uksouth
UK West              ukwest
West Central US      westcentralus
West US 2            westus2
Korea Central        koreacentral
Korea South          koreasouth
France Central       francecentral
France South         francesouth
Australia Central    australiacentral
Australia Central 2  australiacentral2
UAE Central          uaecentral
UAE North            uaenorth
South Africa North   southafricanorth
South Africa West    southafricawest
Switzerland North    switzerlandnorth
Switzerland West     switzerlandwest
Germany North        germanynorth
Germany West Central germanywestcentral
Norway West          norwaywest
Norway East          norwayeast
Brazil Southeast     brazilsoutheast
West US 3            westus3

```


## References

- [Template Documentation](https://docs.microsoft.com/azure/devops/pipelines/process/templates?view=azure-devops)
- [YAML Schema Reference](https://docs.microsoft.com/azure/devops/pipelines/yaml-schema?view=azure-devops&tabs=schema)

## Who do I talk to

- Erik Hooper <Erik.Hooper@se.com>

## License

Copyright © 2020 Telvent USA LLC. All rights reserved.

Unauthorized copying is strictly prohibited. Proprietary and confident
