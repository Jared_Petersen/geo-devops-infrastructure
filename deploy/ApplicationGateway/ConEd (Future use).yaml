ServiceName: ConEd
Environments:
  - Subscription: Prod
    Tenants:
      - TenantAlias: Con Edison Development
        TenantId: ConEdDev
        ResourceGroup: ConEd
        Location: eastus
        IsProductionTenant: false
        Auth0Apps:
          - Name: ArcFM Connectivity Evaluator (OIDC)
          - Name: ArcFM Mobile (OIDC)
            CallBacks:
              - https://arcfmconed.auth0.com/mobile
              - com.telventutilitiesgroup.arcfm://arcfmconed.auth0.com/ios/com.telventutilitiesgroup.arcfm/callback
              - com.schneider-electric.EcoStuxureArcFMMobile://arcfmconed.auth0.com/ios/com.schneider-electric.EcoStuxureArcFMMobile/callback
              - com.schneider-electric.EcoStuxureArcFMMobile://arcfmconed.auth0.com/android/com.schneider-electric.EcoStuxureArcFMMobile/callback
          - Name: ArcFM Mobile Portal (OIDC)
            CallBacks:
              - https://orbit.schneider-electric.com/signin-auth0
          - Name: Designer (OIDC)
          - Name: Editor (OIDC)
          - Name: SE Geospatial Services
          - Name: Solution Center (OIDC)
        Services:
          - Name: Access
            Url: https://coneddev.arcfmsolution.com/Access/
          - Name: Communication
            Url: https://coneddev.arcfmsolution.com/Communication/
          - Name: CuService
            Url: https://coneddev.arcfmsolution.com/CuService/
          - Name: DesignerCoordination
            Url: https://coneddev.arcfmsolution.com/DesignerCoordination/
          - Name: DesignerEquipmentCatalog
            Url: https://coneddev.arcfmsolution.com/DesignerEquipmentCatalog/
          - Name: Editor
            Url: https://coneddev.arcfmsolution.com/Editor/
          - Name: ESRI-Portal-OAuthMob
            ClientId: 5wZQRVbfNRzMyRgw
            Url: https://portaloauthmob.arcfmsolution.com/arcgis/sharing/rest
          - Name: GeodatabaseManager
            Url: https://coneddev.arcfmsolution.com/GeodatabaseManager/
          - Name: KvStorage
            Url: https://coneddev.arcfmsolution.com/KvStorage/
          - Name: Mapping
            Url: https://coneddev.arcfmsolution.com/Mapping/
          - Name: MobileSync
            Url: https://coneddev.arcfmsolution.com/MobileSync/
          - Name: Monitoring
            Url: https://coneddev.arcfmsolution.com/Monitoring/
          - Name: NamedSearch
            Url: https://coneddev.arcfmsolution.com/NamedSearch/
          - Name: PrintTemplatesService
            Url: https://coneddev.arcfmsolution.com/PrintTemplatesService/
          - Name: Redlining
            Url: https://coneddev.arcfmsolution.com/Redlining/
          - Name: ServiceBusManagement
            Url: https://coneddev.arcfmsolution.com/ServiceBusManagement/
          - Name: SessionManager
            Url: https://coneddev.arcfmsolution.com/SessionManager/
          - Name: Tenancy
          - Name: DesignerGrrFunctions
            Url: https://coneddev.arcfmsolution.com/DesignerGrrFunctions/
    Regions:
      - Id: eastus
        Variables:
          NoahTestNatGatewayName: arcfm-natgateway-westus-dev-NoahTest-01
          NoahTestGatewayName: arcfm-gateway-westus-dev-NoahTest-01
          FrontendSubnetName: frontend
          BackendSubnetName: backend
          CommonWestUSVirtualNetworkName: arcfm-common-westus-dev
          CommonWestUSSubnetName: arcfm-common-westus-dev
          CommonWestUSNatGatewayName: arcfm-common-westus-dev
          BackendNSG: arcfm-NoahTest-backend-nsg
          ConEdIpRange: 158.57.0.0/16
          KevinIpRange: "71.211.132.148"
          BackendIpRange: "172.17.1.0/24"
          CICD_IPAddresses: "104.42.29.105,52.158.249.229,13.91.23.167,13.86.231.121,104.42.19.252"
        VNets:
          - Name: $(var::NoahTestGatewayName)
            ResourceGroup: NoahTest
            AddressPrefixes:
              - 172.17.0.0/16
            Subnets:
              - AddressPrefix: 172.17.0.0/24
                Name: $(var::FrontendSubnetName)
                NetworkSecurityGroup: $(var::NoahTestGatewayName)
                ServiceEndpoints:
                  - Service: Microsoft.Web
              - AddressPrefix: $(var::BackendIpRange)
                NetworkSecurityGroup: $(var::BackendNSG)
                NatGateway: $(var::NoahTestNatGatewayName)
                Name: $(var::BackendSubnetName)
                ServiceEndpoints:
                  - Service: Microsoft.ServiceBus
                  - Service: Microsoft.Storage
                Delegations:
                  - Microsoft.Web/serverFarms
          - Name: arcfm-common-westus-dev
            ResourceGroup: common
            AddressPrefixes:
              - 172.17.0.0/16
            Subnets:
              - AddressPrefix: 172.17.0.0/24
                Name: GeospatialServices
                NatGateway: $(var::CommonWestUSNatGatewayName)
                ServiceEndpoints:
                  - Service: Microsoft.ServiceBus
                  - Service: Microsoft.Storage
                Delegations:
                  - Microsoft.Web/serverFarms
              - AddressPrefix: 172.17.1.0/24
                Name: arcfm-common-westus-dev
                NatGateway: $(var::CommonWestUSNatGatewayName)
                ServiceEndpoints:
                  - Service: Microsoft.ServiceBus
                  - Service: Microsoft.Storage
                Delegations:
                  - Microsoft.Web/serverFarms
        NetworkSecurityGroups:
          - Name: $(var::NoahTestGatewayName)
            ResourceGroup: NoahTest
            SecurityRules:
              - Name: Allow_GWM
                DestinationPortRange: 65200-65535
                SourceAddressPrefix: GatewayManager
                Priority: 100
              - Name: Allow_ConEd_Network # used as an example
                Priority: 200
                DestinationPortRange: 443
                SourceAddressPrefix: $(var::ConEdIpRange)
              - Name: Allow_KevinsHome_Network
                Priority: 300
                DestinationPortRange: 443
                SourceAddressPrefix: $(var::KevinIpRange)
              - Name: Allow_BackendSubnet
                Priority: 400
                DestinationPortRange: 443
                SourceAddressPrefix: $(PublicIPAddress::NoahTest::$(var::NoahTestNatGatewayName)::IP)
              - Name: Allow_CommonWestUSSubnet
                Priority: 500
                DestinationPortRange: 443
                SourceAddressPrefix: $(PublicIPAddress::Common::$(var::CommonWestUSNatGatewayName)::IP)
              - Name: Allow_CICD_Machines
                Priority: 600
                DestinationPortRange: 443
                SourceAddressPrefix: $(var::CICD_IPAddresses)
              - Name: Allow_AvailabilityTests
                Priority: 700
                DestinationPortRange: 443
                SourceAddressPrefix: ApplicationInsightsAvailability
          - Name: $(var::BackendNSG)
            ResourceGroup: NoahTest
            SecurityRules:
              - Name: Deny_All # The deny all will guard access to all non-webapp resources.  Web app vnet integration do not obey inbound NSG rules, only outbound rules.
                Access: Deny
                Priority: 100
        Storage:
          - Name: arcfmnoahtestdev
            ResourceGroup: NoahTest
            TenantId: NoahTest
            Kind: StorageV2
            NetworkAcls:
              IpRules:
                - Value: $(var::ConEdIpRange)
                - Value: $(var::KevinIpRange)
                - Value: $(PublicIPAddress::NoahTest::$(var::NoahTestGatewayName)::IP)
              VirtualNetworkRules:
                - SubnetConfiguration:
                    VNet: $(var::NoahTestGatewayName)
                    Subnet: $(var::BackendSubnetName)
                - SubnetConfiguration:
                    ResourceGroup: common
                    VNet: $(var::CommonWestUSVirtualNetworkName)
                    Subnet: $(var::CommonWestUSSubnetName)
        ServiceBus:
          - Name: arcfm-coneddev-premium
            ResourceGroup: NoahTest
            TenantId: NoahTest
            NetworkRuleset:
              IpRules:
                - IpMask: $(var::ConEdIpRange)
                - IpMask: $(var::KevinIpRange)
                - IpMask: $(PublicIPAddress::NoahTest::$(var::NoahTestGatewayName)::IP)
              VirtualNetworkRules:
                - SubnetConfiguration:
                    VNet: $(var::NoahTestGatewayName)
                    Subnet: $(var::BackendSubnetName)
                - SubnetConfiguration:
                    ResourceGroup: common
                    VNet: $(var::CommonWestUSVirtualNetworkName)
                    Subnet: $(var::CommonWestUSSubnetName)
              DefaultAction: Deny
              TrustedServiceAccessEnabled: true
            Topics:
              - Name: jobservices
              - Name: kvupdates
        # AppServicePlans:
        #   - Name: arcfm-NoahTest-westus-dev
        #     ResourceGroup: NoahTest
        #     Size: S1
        #     Instances: 1

        WebApps:
          - Name: arcfm-noahtest-test-access-restriction
            ResourceGroup: NoahTest
            AppServicePlan: arcfm-noahtest-nonprod-westus-dev
            AppSettings:
              WEBSITE_VNET_ROUTE_ALL: 1
            IpSecurityRestrictions:
              - Name: Allow Only Frontend Subnet
                Priority: 100
                Action: Allow
                VNet: $(var::NoahTestGatewayName)
                Subnet: $(var::FrontendSubnetName)
            VNetIntegration:
              VNet: $(var::NoahTestGatewayName)
              Subnet: $(var::BackendSubnetName)

        # WebApps:
        #   - Name: arcfm-noahtest-redlining-dev
        #     ResourceGroup: NoahTest
        #     AppServicePlan: arcfm-noahtest-nonprod-westus-dev
        #     AppSettings:
        #       TenancyService:TenantIds:0: NoahTest
        #       WEBSITE_VNET_ROUTE_ALL: 1
        #     IpSecurityRestrictions:
        #       - Name: Allow Only Frontend Subnet
        #         Priority: 100
        #         Action: Allow
        #         VNet: arcfm-gateway-westus-dev-NoahTest-01
        #         Subnet: frontend
        #     VNetIntegration:
        #       VNet: arcfm-gateway-westus-dev-NoahTest-01
        #       Subnet: backend

        #   - Name: arcfm-NoahTest-test-access-restriction
        #     IpSecurityRestrictions:
        #       - Name: Allow Only Frontend Subnet
        #         Priority: 100
        #         Action: Allow
        #         VNet: $(var::NoahTestGatewayName)
        #         Subnet: $(var::FrontendSubnetName)
        #     VNetIntegration:
        #       VNet: $(var::NoahTestGatewayName)
        #       Subnet: $(var::BackendSubnetName)
        #     ResourceGroup: NoahTest
        #     AppServicePlan: arcfm-NoahTest-westus-dev
        #   - Name: arcfm-NoahTest-test-access-restriction2
        #     IpSecurityRestrictions:
        #       - Name: Allow Only Frontend Subnet
        #         Priority: 100
        #         Action: Allow
        #         VNet: $(var::NoahTestGatewayName)
        #         Subnet: $(var::FrontendSubnetName)
        #     VNetIntegration:
        #       VNet: $(var::NoahTestGatewayName)
        #       Subnet: $(var::BackendSubnetName)
        #     ResourceGroup: NoahTest
        #     AppServicePlan: arcfm-NoahTest-westus-dev
        NatGateways:
          - Name: $(var::NoahTestNatGatewayName)
            ResourceGroup: NoahTest
            PublicIPAddresses:
              - Name: $(var::NoahTestNatGatewayName)
                ResourceGroup: NoahTest
          - Name: $(var::CommonWestUSNatGatewayName)
            ResourceGroup: Common
            PublicIPAddresses:
              - Name: $(var::CommonWestUSNatGatewayName)
                ResourceGroup: Common
        PublicIPAddresses:
          - Name: $(var::NoahTestGatewayName)
            ResourceGroup: NoahTest
          - Name: $(var::NoahTestNatGatewayName)
            ResourceGroup: NoahTest
          - Name: $(var::CommonWestUSNatGatewayName)
            ResourceGroup: Common
        ApplicationGateways:
          - Name: $(var::NoahTestGatewayName)
            ResourceGroup: NoahTest
            VNet: $(var::NoahTestGatewayName)
            Subnet: $(var::FrontendSubnetName)
            PublicIPAddress: $(var::NoahTestGatewayName)
            Autoscale:
              MinCapacity: 0
              MaxCapacity: 5
            Probes:
              - Name: healthProbe
                Path: /health
              - Name: rootProbe
                Path: /
            BackendHttpSettings:
              - Name: healthProbe
                Probe: healthProbe
              - Name: rootProbe
                Probe: rootProbe
            BackendPools:
              - Name: Access
                BackendPoolAddresses:
                  - Target: access.arcfmsolution.com
              - Name: Communication
                BackendPoolAddresses:
                  - Target: communication.arcfmsolution.com
              - Name: ExternalEvents
                BackendPoolAddresses:
                  - Target: arcfm-coned-externalevents.azurewebsites.net
              - Name: KvStorage
                BackendPoolAddresses:
                  - Target: kvstorage.arcfmsolution.com
              - Name: Mapping
                BackendPoolAddresses:
                  - Target: arcfm-mapping-westus-dev.azurewebsites.net
              - Name: NoahTest-MobileSync
                BackendPoolAddresses:
                  - Target: arcfm-noahtest-mobile-sync-dev.azurewebsites.net
              - Name: Monitoring
                BackendPoolAddresses:
                  - Target: semonitoringdev.azurewebsites.net
              - Name: PrintTemplatesService
                BackendPoolAddresses:
                  - Target: arcfm-printing-westus-dev.azurewebsites.net
              - Name: NoahTest-Redlining
                BackendPoolAddresses:
                  - Target: arcfm-noahtest-redlining-dev.azurewebsites.net
              - Name: ServiceBusManagement
                BackendPoolAddresses:
                  - Target: arcfm-servicebus-westus-dev.azurewebsites.net
              - Name: SessionManager
                BackendPoolAddresses:
                  - Target: arcfm-sessionmanager-westus-dev.azurewebsites.net
            RoutingRules:
              - Name: ConEdDev
                HostName: coneddev.arcfmsolution.com # creates a Listener
                DefaultBackendHttpSetting: healthProbe
                Error502PageUrl: https://arcfmcommonwestusdev.blob.core.windows.net/errorpages/html502.html
                # RuleType: PathBasedRouting
                PathRules:
                  - Name: Access
                  - Name: Communication
                  - Name: ExternalEvents
                  - Name: KvStorage
                  - Name: Mapping
                  - Name: ConEd-MobileSync
                    Path:
                      BasePath: mobilesync
                  - Name: Monitoring
                  - Name: ConEd-Redlining
                    Path:
                      BasePath: redlining
                  - Name: ServiceBusManagement
# - Subscription: Prod
#   Regions:
#   - Id: eastus
#     AppServicePlans:
#     - Name: arcfm-adms-eastus
#       ResourceGroup: ADMS
#       Size: S1
#       Instances: 1
